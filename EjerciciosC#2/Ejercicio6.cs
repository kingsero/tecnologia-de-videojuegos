﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio6
{
    class Program
    {
        static void Main(string[] args)
        {
            int numero, resultado = 1;

            Console.WriteLine("Inserta un número para factorizarlo: ");
            numero = int.Parse(Console.ReadLine());

            for (int i = 1; i <= numero; i++)
            {
                resultado = resultado * i;
            }
            Console.WriteLine("El factorial de " + numero + " es: " + resultado);
            Console.ReadLine();
        }
    }
}
