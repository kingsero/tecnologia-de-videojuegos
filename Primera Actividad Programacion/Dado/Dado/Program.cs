﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dado
{
    class Program
    {
        static void Main(string[] args)
        {
            Random dado = new Random ();
            int[] array = new int[6];
            for (int intento = 0; intento < 50; intento++)
            {
                int aleatorio = dado.Next(0, 6);

                if (aleatorio == 1)
                    array[0] += 1;
                if (aleatorio == 2)
                    array[1] += 1;
                if (aleatorio == 3)
                    array[2] += 1;
                if (aleatorio == 4)
                    array[3] += 1;
                if (aleatorio == 5)
                    array[4] += 1;
                if (aleatorio == 6)
                    array[5] += 1;

            }
            Console.WriteLine("El número 1 se repite {0}", array[0]);
            Console.WriteLine("El número 2 se repite {0}", array[1]);
            Console.WriteLine("El número 3 se repite {0}", array[2]);
            Console.WriteLine("El número 4 se repite {0}", array[3]);
            Console.WriteLine("El número 5 se repite {0}", array[4]);
            Console.WriteLine("El número 6 se repite {0}", array[5]);

            Console.ReadLine();
        }
    }
}
