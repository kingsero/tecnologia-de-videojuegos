﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;


//Este es un ej que vi por internet, tenia varios fallos y consegui arreglarlo, aunque no esta en las actividades consideré que no estaba de más mandarlo.


namespace Ejercicio8
{
    class Program
    {
        static void Main()
        {
            string mensaje = "Hola";
            Console.WriteLine("Esto es el mensaje sin cifrar: " + mensaje);
            Console.WriteLine("Pulse una tecla para continuar");
            Console.ReadKey();
            SymmetricAlgorithm algoritmo = SymmetricAlgorithm.Create("Rijndael");
            ConfigurarAlgoritmo(algoritmo);
            GenerarClave(algoritmo);
            GenerarIV(algoritmo);
            byte[] mensajeEncriptado = Encriptar (mensaje, algoritmo);
            Console.WriteLine("Esto es el mensaje encriptado");
            foreach (byte b in mensajeEncriptado)
            {
                Console.Write("{0:X2} ", b);
            }
            Console.WriteLine("\nPulse una tecla para continuar…\n");
            Console.ReadKey();
            byte[] mensajeDesencriptado = Desencriptar (mensajeEncriptado, algoritmo);
            string mensajeDescrifrado = Encoding.UTF8.GetString(mensajeDesencriptado);
            Console.WriteLine("Esto es el mensaje descifrado: " +mensajeDescrifrado);
            Console.WriteLine("Pulse una tecla para terminar…\n");
            Console.ReadKey();
            algoritmo.Clear();
        }

        private static void ConfigurarAlgoritmo(SymmetricAlgorithm algoritmo)
        {
          
            algoritmo.BlockSize = 128;
         
            algoritmo.Mode = CipherMode.CBC;
            algoritmo.Padding = PaddingMode.PKCS7;
            Console.WriteLine("Longitud de bloque: {0}", algoritmo.BlockSize);
            Console.WriteLine("Modo de cifrado: {0}", algoritmo.Mode);
            Console.WriteLine("Modo de relleno: {0}", algoritmo.Padding);
            Console.WriteLine("Pulse una tecla para continuar…\n");
            Console.ReadKey();
        }
        private static void GenerarClave(SymmetricAlgorithm algoritmo)
        {
            algoritmo.KeySize = 256;
            Console.WriteLine("Longitud de la clave:   {0}", algoritmo.KeySize);
            Console.WriteLine("Pulse una tecla para continuar…\n");
            Console.ReadKey();
            Console.WriteLine("La clave: ");
            foreach (byte b in algoritmo.Key)
            {
                Console.Write("{0:X2} ", b);
            }
            Console.WriteLine("\nPulse una tecla para continuar…\n");
            Console.ReadKey();
            algoritmo.GenerateKey();
            Console.WriteLine("Otra clave: ");
            foreach (byte b in algoritmo.Key)
            {
                Console.Write("{0:X2} ", b);
            }
            Console.WriteLine("\nPulse una tecla para continuar…\n");
            Console.ReadKey();
            RandomNumberGenerator randomNumberGenerator = RandomNumberGenerator.Create();
            randomNumberGenerator.GetBytes(algoritmo.Key);
            Console.WriteLine("Otra forma de obtener una clave: ");
            foreach (byte b in algoritmo.Key)
            {
                Console.Write("{0:X2} ", b);
            }
            Console.WriteLine("\nPulse una tecla para continuar…\n");
            Console.ReadKey();
        }
        private static void GenerarIV(SymmetricAlgorithm algoritmo)
        {
            algoritmo.GenerateIV();
            Console.WriteLine("IV(Vector de inicialización): ");
            foreach (byte b in algoritmo.IV)
            {
                Console.Write("{0:X2} ", b);
            }
            Console.WriteLine("\nPulse una tecla para continuar…\n");
            Console.ReadKey();
        }
        public static byte[] Encriptar(string mensajeSinEncriptar, SymmetricAlgorithm algoritmo)
        {
            ICryptoTransform encriptador = algoritmo.CreateEncryptor();
            byte[] textoPlano = Encoding.Default.GetBytes(mensajeSinEncriptar);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encriptador, CryptoStreamMode.Write);
            cryptoStream.Write(textoPlano, 0, textoPlano.Length);
            cryptoStream.FlushFinalBlock();
            memoryStream.Close();
            cryptoStream.Close();
            return memoryStream.ToArray();
        }
        public static byte[] Desencriptar(byte[] mensajeEncriptado, SymmetricAlgorithm algoritmo)
        {
            int numeroBytesDesencriptados = 0;
            byte[] mensajeDesencriptado = new byte[mensajeEncriptado.Length];
            ICryptoTransform desencriptador = algoritmo.CreateDecryptor();
            MemoryStream memoryStream = new MemoryStream(mensajeEncriptado);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, desencriptador, CryptoStreamMode.Read);
            numeroBytesDesencriptados = cryptoStream.Read(mensajeDesencriptado, 0, mensajeDesencriptado.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return mensajeDesencriptado;
        }

    }
    
}
