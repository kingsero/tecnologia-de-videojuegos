﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio7
{
    class Program
    {
        static void Main(string[] args)
        {
            string a = string.Empty;
            Console.WriteLine("Introduce una palabra");
            a = Console.ReadLine();

            char[] temp = a.ToCharArray();
            Array.Reverse(temp);
            string b = new string(temp);

            if (a.ToLower().Equals(b.ToLower()))
            {
                Console.WriteLine("Es palíndromo");
            } else
            {
                Console.WriteLine("No es palíndromo");
            }
            Console.ReadLine();



        }
    }           
}
