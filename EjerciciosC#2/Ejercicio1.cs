﻿using System;
using System.IO;

class CountLetters{
static void Main(){
        Console.Write("Name of file: ");
        string nameFile = Console.ReadLine();

        Console.Write("Letter for count: ");
        string letter = Console.ReadLine();

        StreamReader myfile;
        myfile = File.OpenText(nameFile);


        string line;
        int countLetter = 0;
        do {
            line = myfile.ReadLine();
            if (line != null)
            {
                for (int i = 0; i < line.Length; i++)
                {
                    if (line.Substring(i, 1) == letter)
                    {
                        countLetter++;
                    }
                }
            }
        } while (line != null);
        myfile.Close();
        Console.WriteLine("Amount of letter: {0}", countLetter);
    }  
}   
