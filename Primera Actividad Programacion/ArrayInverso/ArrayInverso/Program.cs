﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayInverso
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] Datos = { 1,2,3,4,5};

            Console.WriteLine("El Array de entrada es: ");
            for (int ent = 0; ent < Datos.Length; ent++)
                Console.WriteLine( Datos[ent].ToString() + ", ");
       
            Array.Sort(Datos);
            Array.Reverse(Datos);

                
                Console.Write("El Array de salida es: ");
            for (int cont = 0; cont < Datos.Length; cont++)
                Console.Write( Datos[cont].ToString() + ", "); 
               
                Console.ReadKey();

        }
    }
}
