﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notas
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce tu nota: ");
            int nota = int.Parse(Console.ReadLine());
            switch (nota)
            {
                case (10):
                case (9):

                    Console.WriteLine("Sobresaliente");
                    break;

                case (8):
                case (7):

                    Console.WriteLine("Notable");
                    break;

                case (6):
                case (5):

                    Console.WriteLine("Suficiente");
                    break;

                default:
                    Console.WriteLine("Suspenso");
                    break;



            }
        }
    }
}
