﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Texto: ");
            string texto = Console.ReadLine();
            var letras = texto.ToCharArray();
            int[] array = new int[letras.Length];
            for (int i = 0; i < letras.Length; i++)
            {
                if (letras[i] == ' ')
                {
                    letras[i] = '_';
                }
                if (letras[i] == 'a' || letras[i] == 'e' || letras[i] == 'i' || letras[i] == 'o' || letras[i] == 'u')
                {
                    letras[i] -= (char)32;
                }
                array[i] = letras[i];
                Console.Write(array[i]);

            }
            Console.WriteLine();
            for (int j = 0; j < letras.Length; j++)
            {
                Console.Write(letras[j]);
            }
            Console.ReadKey();
        }
    }
}
