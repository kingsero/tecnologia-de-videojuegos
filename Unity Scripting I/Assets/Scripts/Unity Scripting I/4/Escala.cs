﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escala : MonoBehaviour
{
    public float UnidadEscala = 5f;

    void Start()
    {
        transform.localScale = new Vector3(transform.localScale.x + UnidadEscala * Time.deltaTime, 1, 1);
        Debug.Log("Rotating From Start Evet");
    }

    void Update()
    {
        transform.localScale = new Vector3(transform.localScale.x + UnidadEscala * Time.deltaTime, 1, 1);
        Debug.Log("Rotating From Update Event");
    }
}