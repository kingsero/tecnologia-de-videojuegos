﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio4
{
    public class PointAsStruct
    {
            struct Point
        {
            public short x;
            public short y;
            public byte r;
            public byte g;
            public byte b;

        }
        public static void Main ()
        {
            Point p1, p2;

            Console.Write("Enter X for the first point: ");
            p1.x = Convert.ToInt16(Console.ReadLine());
            Console.Write("Enter Y for the first point: ");
            p1.y = Convert.ToInt16(Console.ReadLine());
            Console.Write("Enter Red for the first point: ");
            p1.r = Convert.ToByte(Console.ReadLine());
            Console.Write("Enter Green for the first point: ");
            p1.g = Convert.ToByte(Console.ReadLine());
            Console.Write("Enter Blue for the first point: ");
            p1.b = Convert.ToByte(Console.ReadLine());

            Console.Write("Enter X for the first point: ");
            p2.x = Convert.ToInt16(Console.ReadLine());
            Console.Write("Enter Y for the first point: ");
            p2.y = Convert.ToInt16(Console.ReadLine());
            Console.Write("Enter Red for the first point: ");
            p2.r = Convert.ToByte(Console.ReadLine());
            Console.Write("Enter Green for the first point: ");
            p2.g = Convert.ToByte(Console.ReadLine());
            Console.Write("Enter Blue for the first point: ");
            p2.b = Convert.ToByte(Console.ReadLine());

            Console.WriteLine(
                "P1 is located in ({0}, {1}), colour ({2}, {3}, {4})", p1.x, p1.y, p1.r, p1.g, p1.b);
            Console.WriteLine(
                "P2 is located in ({0}, {1}), colour ({2}, {3}, {4})", p2.x, p2.y, p2.r, p2.g, p2.b);
           

        }
  
    } 
   
}
