﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerWithInputv2 : MonoBehaviour
{
    public float speed;
    public float runSpeed;
    public bool hRotacion;
    private float hInput, vInput;
    float step;

    void Update()
    {
        step = speed;
        hInput = Input.GetAxisRaw("Horizontal");
        vInput = Input.GetAxisRaw("Vertical");

        if (vInput != 0)
        {
            transform.Translate(vInput * Vector3.forward * Time.deltaTime * step);
        }
        if (hInput != 0)
        {
            if (!hRotacion)
            {
                transform.Translate(hInput * Vector3.right * Time.deltaTime * step);
            }
            else
            {
                transform.Rotate(hInput * Vector3.right * 55 * Time.deltaTime * step);
            }
        }
    }
}



