﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciSerie
{
    class Program
    {
        static void Main(string[] args)
        {
           
            int a, b, n, i, aux;
            
            n = 25;
            a = 0;
            b = 1; 
            for (i = 0; i < n; i++)  
            {
                aux = a;
                a = b; 
                b = aux + a; 
                Console.WriteLine(a); 
            }
            Console.ReadKey();

        }
    }
}

