﻿using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject objectToSpawn;
    float time;
    void Start()
    {
        time = 0;

        Renderer rend = GetComponent<Renderer>();
        rend.material.shader = Shader.Find("Specular");
        rend.material.SetColor("_Color", Color.red);


    }
    void Update()
    {
        time += Time.deltaTime;

        transform.RotateAround(Vector3.zero, Vector3.up, 20 * Time.deltaTime);
        if (time > 1.5)
        {
            time = 0;
            GameObject objAux = Instantiate(objectToSpawn, transform.position, Quaternion.identity) as GameObject;
            float scale = Random.Range(1, 10);
            objAux.transform.localScale = new Vector3(scale, scale, scale);
            Rigidbody rb = objAux.GetComponent<Rigidbody>();
            rb.drag = Random.Range(0f, 0.1f);
            rb.mass = Random.Range(1, 10);
            rb.AddForce(Vector3.right * Random.Range(10, 150), ForceMode.Impulse);

        }
        
    }
}
  
    

