﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueFalse
{
    class Program
    {
        static bool comprobar(int[,] array, int numero)
        {
            for (int i = 0; i < 2; i++)
            {
                for (int c = 0; c < 2; c++)
                {
                    if (array[i, c] == numero)
                        return true;
                }
            }
            return false;
        }
        static void Main(string[] args)
        {
            int[,] array = new int[2, 2] { { 5, 1 }, { 3, 5 } };
            Console.WriteLine("Inserta un numero: ");
            int numero = int.Parse(Console.ReadLine());
            if (comprobar(array, numero))
            {
                Console.WriteLine("Está en el array");
            }
            else
            {
                Console.WriteLine("No está en el array");
            }
            Console.ReadKey();
        }
    }
}