﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    class Program
    {
        static void minMaxArray(int[] array, ref int min, ref int max)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < min)
                    min = array[i];
                if (array[i] > max)
                    max = array[i];
            }
        }

        static void Main(string[] args)
        {
            int Max = 0;
            int Min = 0;
            int[] lista = { 1, 2, 3, 4 };
            minMaxArray(lista, ref Min, ref Max);
            Console.WriteLine("El mínimo es {0} ", Min);
            Console.WriteLine("El máximo es {0} ", Max);
            Console.ReadKey();
        }

    }
}