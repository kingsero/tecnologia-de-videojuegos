﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio10
{
    class Program
    {
        static void Main(string[] args)
        {
            int b, e, i, sol = 1;
            Console.WriteLine("Introduce la base de la potencia");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduce el exponente de la potencia");
            e = int.Parse(Console.ReadLine());
            
            for (i = 0; i < e; i++)
            {
                sol = sol * b;
            }
            Console.WriteLine("El resultado de la potencia de " + b + " elevado a " + e + " es: " + sol);
            Console.ReadLine();
        }
    }
}
