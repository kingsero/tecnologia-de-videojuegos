﻿
using System;

namespace Sumas
{
    class Program
    {
        static void Main(string[] args)
        {
            int Numero;
            int SumaPar = 0;
            int SumaImpar = 0;
            for (int i = 1; i <= 499; i++)
            {
                if (i % 2 == 0)
                {
                    SumaPar = SumaPar + i;
                }
                else
                {
                    SumaImpar = SumaImpar + i;
                }

            }

            Console.WriteLine("La suma de los Numeros Pares es:" + SumaPar);
            Console.WriteLine("La suma de los Numeros Impares es:" + SumaImpar);
            Console.ReadLine();

        }
    }
} 

