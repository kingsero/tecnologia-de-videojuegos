﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getRandomArray
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("¿Cuál quieres que sea la longitud del Array?");
            int longitud = int.Parse(Console.ReadLine());
            int [] randomArray = getRandomArray(longitud, 0, 30);
            for (int y = 0; y < longitud; y++)
            {
                Console.WriteLine(randomArray[y]);
            }

            Console.ReadKey();

        }
        static int[] getRandomArray(int tamanio, int min, int max)
        {
            int[] Array = new int[tamanio];
            Random aleatorio = new Random(); 
            for (int i = 0; i < tamanio; i++)
            {
                Array[i] = aleatorio.Next(min, max);
            }
            return Array;
        }
    }
}
