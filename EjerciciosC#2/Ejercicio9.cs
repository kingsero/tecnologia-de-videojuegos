﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio9
{
    class Program
    {
        static int columna;
        static int fila;
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce un texto: ");
            string text = Console.ReadLine();
            Console.WriteLine("La distancia en x: ");
            int x = int.Parse(Console.ReadLine());
            Console.WriteLine("La distancia en y: ");
            int y = int.Parse(Console.ReadLine());

            try
            {
                Console.SetCursorPosition(columna + x, fila + y);
                Console.Write(text);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.Clear();
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();
        }
    }
}
