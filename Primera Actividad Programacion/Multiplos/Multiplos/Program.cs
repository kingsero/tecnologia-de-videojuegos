﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multiplos
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int h = 0; h <= 100; h++)
            {
                if (h % 3 != 0 && h % 5 != 0)
                    Console.WriteLine(h);

            }
            Console.ReadKey();

        }
    }
}
