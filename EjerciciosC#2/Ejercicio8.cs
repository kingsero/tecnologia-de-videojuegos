﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio8
{
    public static class Encriptator
    {

        public static void Encriptar(ref string texto)
        {
            char x;
            char y;
            var ss = texto.ToCharArray();

            for (int i = 65; i < 91; i++)
            {

                y = (char)i;
                if (ss.Contains(y))
                {
                    x = (char)(i + 1);

                    texto = texto.Replace(y, x);
                }

            }
            for (int i = 97; i < 123; i++)
            {

                y = (char)i;
                if (ss.Contains(y))
                {
                    x = (char)(i + 1);

                    texto = texto.Replace(y, x);
                }
            }
        }



        public static void Desencriptar(ref string texto)
        {
            char x;
            char y;
            var kk = texto.ToCharArray();


            for (int i = 65; i < 91; i++)
            {

                y = (char)i;
                if (kk.Contains(y))
                {
                    x = (char)(i - 1);

                    texto = texto.Replace(y, x);
                }
            }
            for (int i = 97; i < 123; i++)
            {

                y = (char)i;
                if (kk.Contains(y))
                {
                    x = (char)(i - 1);
                    texto = texto.Replace(y, x);
                }
            }
        }
    }


    class Program
    {
        static void Main(string[] args)

        {
            Console.WriteLine("Introduce un texto");

            string texto = Console.ReadLine();
            Encriptator.Encriptar(ref texto);
            Console.WriteLine(texto);
            Encriptator.Desencriptar(ref texto);
            Console.WriteLine(texto);
            Console.ReadKey();
        }
    }
}